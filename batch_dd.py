#!/usr/bin/env python2

import os
import sys
import subprocess
import multiprocessing
from multiprocessing import Pool
from multiprocessing import Manager
import functools
from functools import partial

def dd_drive(p1_drive,p2_drive):
	print(p1_drive)
	print(p2_drive)

WORKER_COUNT=4

pool1_disks = open("pool1_disks").read().splitlines()
pool2_disks = open("pool2_disks").read().splitlines()

drives_dict=dict(zip(pool1_disks,pool2_disks))
print(drives_dict)

pool = Pool(processes=WORKER_COUNT)
pool.map(dd_drive, [(p1_drive, p2_drive) for p1_drive in drives_dict.keys() + drives_dict.values()])

pool.close()
pool.join()