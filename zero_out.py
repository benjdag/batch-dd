#!/usr/bin/env python2

import os
import sys
import errno
import fcntl
import time
import signal
import logging
import logging.handlers
import subprocess
import multiprocessing
from multiprocessing import Pool
from multiprocessing import Manager
import functools
from functools import partial

import re
import stat
import logging.handlers as handlers


class SizedTimedRotatingFileHandler(handlers.TimedRotatingFileHandler):
    """
    Handler for logging to a set of files, which switches from one file
    to the next when the current file reaches a certain size, or at certain
    timed intervals
    """

    def __init__(self, filename, maxBytes=0, backupCount=0, encoding=None,
                 delay=0, when='h', interval=1, utc=False):
        handlers.TimedRotatingFileHandler.__init__(
            self, filename, when, interval, backupCount, encoding, delay, utc)
        self.maxBytes = maxBytes

    def shouldRollover(self, record):
        """
        Determine if rollover should occur.

        Basically, see if the supplied record would cause the file to exceed
        the size limit we have.
        """
        if self.stream is None:                 # delay was set...
            self.stream = self._open()
        if self.maxBytes > 0:                   # are we rolling over?
            msg = "%s\n" % self.format(record)
            # due to non-posix-compliant Windows feature
            self.stream.seek(0, 2)
            if self.stream.tell() + len(msg) >= self.maxBytes:
                return 1
        t = int(time.time())
        if t >= self.rolloverAt:
            return 1
        return 0

LOG_LEVEL = logging.DEBUG
CONS_LOG_LEVEL = logging.INFO
FILE_LOG_LEVEL = logging.DEBUG


logger =  logging.getLogger()



def setup_logging(logfile, is_verbose=False):
    """ Setup logging.
    Args:
        logfile (str): Path to the log file
        is_verbose (bool, optional): If logging would be verbose or not
    """

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s] \
(%(levelname)s,%(lineno)d)\t: %(message)s')

    # Check verbosity for console
    if is_verbose:
        global CONS_LOG_LEVEL
        CONS_LOG_LEVEL = logging.DEBUG

    # Setup console logging
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(CONS_LOG_LEVEL)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # Setup file logging
    fh = SizedTimedRotatingFileHandler(logfile, maxBytes=5242880, when='s', interval=300, backupCount=5, encoding='bz2')
    fh.doRollover()
    fh.setLevel(FILE_LOG_LEVEL)
    fh.setFormatter(formatter)
    logger.addHandler(fh)


def dd_drive(p1_drive,p2_drive):
	logger.info(p1_drive + ' to ' + p2_drive)
	cmd2run = ['/bin/dd', 'if=/dev/zero', 'of='+p2_drive, 'status=progress', 'bs=4M']
	logger.info('command: {}'.format(cmd2run))
	dd_process = subprocess.Popen(cmd2run,
								stdout=subprocess.PIPE,
								stderr=subprocess.STDOUT)
	while dd_process.poll() is None:
		time.sleep(30)
		dd_process.send_signal(signal.SIGUSR1)
		output = ''
		while 1:
			output += dd_process.stdout.readline()
			if 'bytes' in output:
				break
		logger.info("command: {} out: {}".format(cmd2run, output.strip()))
	rc = dd_process.poll()
	return rc

def dd_helper(args):
	return dd_drive(*args)

def parallel_dd(list_a, list_b):
	# spark given number of processes
	p = Pool(4) 
	# set each matching item into a tuple
	job_args = [(item_a, list_b[i]) for i, item_a in enumerate(list_a)] 
	# map to pool
	p.map(dd_helper, job_args)

pool1_disks = open("all_pool2_disks").read().splitlines()
pool2_disks = open("all_pool2_disks").read().splitlines()

setup_logging("zero_out.log", True)

parallel_dd(pool1_disks,pool2_disks)